

# **Final Project Fire Eye**

This week I worked on defining my final project idea and started to getting used to the documentation process.

![](../presentation.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/RejHl7Z6yLk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



# **Questions to answer**

## **What will it do?**

We can use the portable life stream case for covering the accidents from all sides. The
commanders will have full view on the accidents to run and give the right orders. The video will
be taking from the accidents will also help in investigations after the accidents. It helps the
trainees before going to the field and facing a real accident. Gives accurate and real time
intelligence as to the progression of the fire spread.

## **Who’s done it beforehand?**

There were phones that show livestream videos through apps but in my experience I never saw a
livestream case being made and used.

## **What will you design?**

I need to design a case that is portable in size and you can fit it in your vehicle and has support where you can hold it and carry it around. It also needs an arched stand that you can hook in the vehicle so you can film without the case moving in place.

## **What materials and components will be used?**

* Modem

* 12V  15000mAh li-ion battery

* Rotatable Camera

* Circuit board that battery checker (RGB LED lights, buzzer, cover for the LED made by 3D
printer)

* Aluminum 3 inch Tube

* Tube foundation to hold the tube in the case.

* Wires

* Switch

* Battery charger

* Assembling parts (Screwdriver and nut)

* Battery mold

* Camera foundation

* Acrylic pieces for covering the design inside of the case.

## **Where will they come from?**

- I will buy the components and materials online or locally. I will fabricate some materials by my
own in the FabLab.

- Tube, stopper motor, wood, electronic components, RGB Light: FabLab.

- Camera, modem, battery: Online purchase.

## **How much will they cost?**



![](../images/week13/LIST.jpg)

##### **Total amount 1653.62**


## **What parts and systems will be made?**

I will make most of the components in the lab

* The portable case

* Camera foundation

* Tube holder

* Board

* Motor Driver


## **What processes will be used?**

1-3D designing and printing - The camera holder.

2- Electronics design and fabrication - designing the main board using Eagle, then fabricating using flatcam and roland.

3-3D designing and milling - designing the case, and fabricating it using the shopbot.

4- Laser cutting - used laser to cut acrylic pieces for covering the design inside of the case.

5- Electronics integration and programming - connected the inputs and outputs to my board, integrated the camera and the motor and programmed the RGB LED.



## **What questions need to be answered?**

Checking whether the signal from my portable case camera is working properly from my
location to the operation room of the Civil Defense. I want to see if the camera can show both
video and sound properly so the operation room can see the incident clearly.

## **How will it be evaluated?**

First I carry the case to the place I want to film, then I switch it on. The camera will come out of the case and the modem will take 5-10 seconds to turn on the LTE in the SIM card and all systems will switch on. The camera will start live streaming and sending the video to the Dubai Civil Defense Operation Room where they can watch the video live.

## **What are the implications?**

We can use the portable life stream case for covering the accidents from all sides. The commanders will have a full view on the accidents to run and give the right orders. The video will be taking form the accidents and will also help in investigations after the accidents. It helps the trainers before going to the field and facing a real accident. Gives accurate and real time intelligence as to the progression of the fire spread.



### **Planning and Sketching**



I started my final project by making a sketch with the measurements as shown below. While making the final project and sketch I had these points in mind:



1.	It must save space in the vehicle.

2.	It is designed to have a curved space for 4 cases to fit in one stand that allows it to be positioned in any place and record without any problem.

3.	It should have free space inside the case to prevent any damage to the camera.

4.	It should have more space for the battery to extend the time for it to stay working.

5.	The sides should be made of acrylic matte that helps show the color of the LED.

![1](../images/week20/1.jpg)
)


To start the designing process, I first drew a simple sketch to determine the general shape. I chose this design, because I think the case will be easily stored in the car and moved to be carried and used for the civil defense. Moreover, the curves in the case are desiged to be milled and assembled neatly.
My aim is to create single frames then attach them together to create the case altogether. I designed the sketch in mind using fusion 360 as shown below:

![1](../images/week20/aaaaaaaaaaaaaaaaaaaaaaaaa.jpg)


I made a model scale to test my project Using the laser machine and its scale .

![1](../images/week20/5.jpg)

After I made a rough sketch of the project, I started to draw on Fusion and I have designed 6 different parts for the case and later I will glue them together.

### **Plywood Case:**

![1](../images/week20/2.jpg)
)

1-	Full plywood case

2-	Border made of plywood with an opening for the camera.

3-	Border of plywood with flash, an opening for the camera, and a holder for the case.

4-	Closed border of plywood to add acrylic inside the frame.

5-	Border of plywood with an opening for the camera and an opening for controlling the switches of the case.

6-	Closed plywood border with magnetic door.

I prepared the design for milling by projecting each of the 2D shapes, and then saving the sketches as DXF files.


![1](../images/week20/dxffile.jpg)

![1](../images/week20/3dmodelcase.jpg)





I placed the settings onto the shopbot machine and created the toolpath for cutting using vcarve, to fabricate the parts for my case. For the settings please see my [computer controlled machining page](computer controlled machining page](http://fabacademy.org/2020/labs/uae/students/ali-binghulaita/assignments/week08/)
)



![1](../images/week20/Screenshot (87)a.jpg)




After inputting the settings onto the shopbot machine and it started cutting the parts for my camera.
for the settings please see my [computer controlled machining page](http://fabacademy.org/2020/labs/uae/students/ali-binghulaita/assignments/week08/)


![1](../images/week20/3.jpg)

The machine finished cutting the main parts for my project and now I have to snap off the pieces and align them together in the pictures below.

![1](../images/week20/4.jpg)
![1](../images/week20/6.jpg)

I started assembling the pieces together and sticked them.

![1](../images/week20/7.jpg)

![1](../images/week20/8.jpg)

I have applied primer on my plywood and after it I have painted it white and the end product is shown below.

![1](../images/week20/9.jpg)

![1](../images/week20/10.jpg)

### **Electronic Work**

I made the board design on Eagle and the picture of the board and schematic can be shown below. I will also put my Eagle files and the bottom of the page.

![1](../images/week20/20-board1.jpg)

![1](../images/week20/20-schematic1.jpg)


I used FlatCam to create all the files that are necessary for the board. The type of files that are available in the program  are Gerber which is used for outlines and traces of the border and Excellon which is used for the drill.

To make the board design I inputted the following settings in the programs I got from
[maha](http://fabacademy.org/2020/labs/uae/students/meha-hashmi/week6/electronics_design.html)


* Tool diameter: 0.4 mm

* Number of passes: 4

* Passes Overlap: 60%

And after I inputted everything I check on the combine check box.

To make the CNC object files I have to input the following settings:

* Cut Z: -0.1

* End Move Z: 60

* Feedrate X-Y: 240

* Feedrate Z: 80

To make the Isolation Geometry I inputted these settings:

* Tool Diameter: 0.8 mm

* Number of Passes: 1

* Passes Overlap: 60%

And after inputting everything I have to check the combine and external isolation checkboxes.

To make the CNC object file I inputted these settings:

* Cut Z: -1.7mm

* Check Multi-Depth Value: 0.7

* End Move Z: 60

* Feedrate X-Y: 240

* Feedrate Z: 80


### **Electronics**

For the electronics section of the project I have used these components that are listed in the picture below.

![1](../images/week20/11.jpg)

I inputted the settings in the machine and it started making the board.

![1](../images/week20/12.jpg)

The board is made and ready to add the components on it.

![1](../images/week20/13.jpg)

I used the solder to fix the components of the board onto it.

![1](../images/week20/14.jpg)
![1](../images/week20/15.jpg)

I added the chips.

![1](../images/week20/16.jpg)

This picture shows a description of the stepper motor driver board and the motor sensor and how it works.

![1](../images/week20/17.jpg)

This picture shows a sketch of the connections and the power of the stepper motor driver board and the power source.

![1](../images/week20/18.jpg)
![1](../images/week20/19.jpg)


A4988 Driver

![1](../images/week20/27.jpg)

1.	Plan A: Motor driver A49535

2.  12 V Output

3.  Input 12 V

4. 12V battery voltage meter with resistor 5k and resistor 10k and I got the information from the video

 <iframe width="560" height="315" src="https://www.youtube.com/embed/ah52N4LlqN8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

5. LED output



The motor driver I tried designing didn’t not work, so I used a ready-made motor driver for the motor I am using in the case

![1](../images/week20/28.jpg)
![1](../images/week20/29.jpg)
![1](../images/week20/30.jpg)

### The connection





![1](../images/week20/PORD2.jpg)

Inputs and Outputs Pins to Atemga328p board:

1. **RGB LED:** is connected to pin **PB0** in Atemga328p board.

2. **LTE Modem and CAM:** is connected directly to the power pins of the ATemga 328p board.

3. **Switch** is connected to pin **PD1** in Atemga328p board.

4. **Stepper motor:** is connected to the motor drive board.

|Motor drive A4988 board pins| Stepper motor pins|
|-------------------------|:---------------------:|
| MS3                     |2B|
| MS2                     |2A|
| MS1                     |1A|
| ENABLE                  |1B|

5. Motor drive A4988:

|Motor drive A4988 board pins| Atemga 328p board pins|
|------------------------------|:---------------------:|
| DIR                          |PC4|
| STEP                         |PC3|


Note:

- I tested the operation of the motor driver and the stepper motor back on the [output devices](http://fabacademy.org/2020/labs/uae/students/ali-binghulaita/assignments/week12/), using an arduino due to the pandamic.

- I have not used the embedded motor drivers on the board (as displayed on the image, I have connected the pins directly) due the overload on the embedded motor drivers. As the motor could not run, but instead make a ticking sound indicating an overload. I have thus used an external motor driver to connect to my stepper motor.






### **Programming**

I used the following code to test the RGB LED strip I used for my final project. I set the output pin of the LED strip and tested it to display the colors green and red using Arduino IDE.

```
/ NeoPixel test program showing use of the WHITE channel for RGBW
// pixels only (won’t look correct on regular RGB NeoPixel strips).
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
 #include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif
#define LED_PIN     6 // Which pin on the Arduino is connected to the NeoPixels?
#define LED_COUNT  10 // How many NeoPixels are attached to the Arduino?
#define BRIGHTNESS 50 // NeoPixel brightness, 0 (min) to 255 (max)
Adafruit_NeoPixel strip(LED_COUNT, LED_PIN, NEO_GRBW + NEO_KHZ800);
void setup() {
  clock_prescale_set(clock_div_1);
  strip.begin();           // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();            // Turn OFF all pixels ASAP
  strip.setBrightness(50); // Set BRIGHTNESS to about 1/5 (max = 255)
}
void loop() {
  // Fill along the length of the strip in various colors...
 	 colorWipe(strip.Color(255,   0,   0)     , 50); // Red
	delay(2000);
  	colorWipe(strip.Color(  0, 255,   0)     , 50); // Green
	delay(2000);
}
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel’s color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}
    strip.show(); // Update strip with new contents
    // There’s no delay here, it just runs full-tilt until the timer and
    // counter combination below runs out.
    firstPixelHue += 40; // Advance just a little along the color wheel
    if((millis() - lastTime) > whiteSpeed) { // Time to update head/tail?
      if(++head >= strip.numPixels()) {      // Advance head, wrap around
        head = 0;
        if(++loopNum >= loops) return;
      }
      if(++tail >= strip.numPixels()) {      // Advance tail, wrap around
        tail = 0;
      }
      lastTime = millis();                   // Save time of last movement
    }
  }
}

```


<iframe width="560" height="315" src="https://www.youtube.com/embed/tFHqyEPgcgs" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



I used the following code from [stoper motor code](https://www.makerguides.com/a4988-stepper-motor-driver-arduino-tutorial/) to test the motor driver and stepper motor.







```
/*Example sketch to control a stepper motor with A4988 stepper motor driver and Arduino without a library. More info: https://www.makerguides.com */

// Define stepper motor connections and steps per revolution:
#define dirPin 3
#define stepPin 4
#define stepsPerRevolution 200

void setup() {
  // Declare pins as output:
  pinMode(stepPin, OUTPUT);
  pinMode(dirPin, OUTPUT);
}

void loop() {
  // Set the spinning direction clockwise:
  digitalWrite(dirPin, HIGH);

  // Spin the stepper motor 1 revolution slowly:
  for (int i = 0; i < stepsPerRevolution; i++) {
    // These four lines result in 1 step:
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(2000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(2000);
  }

  delay(1000);

  // Set the spinning direction counterclockwise:
  digitalWrite(dirPin, LOW);

  // Spin the stepper motor 1 revolution quickly:
  for (int i = 0; i < stepsPerRevolution; i++) {
    // These four lines result in 1 step:
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(1000);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(1000);
  }

  delay(1000);

  // Set the spinning direction clockwise:
  digitalWrite(dirPin, HIGH);

  // Spin the stepper motor 5 revolutions fast:
  for (int i = 0; i < 5 * stepsPerRevolution; i++) {
    // These four lines result in 1 step:
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);

  // Set the spinning direction counterclockwise:
  digitalWrite(dirPin, LOW);

  //Spin the stepper motor 5 revolutions fast:
  for (int i = 0; i < 5 * stepsPerRevolution; i++) {
    // These four lines result in 1 step:
    digitalWrite(stepPin, HIGH);
    delayMicroseconds(500);
    digitalWrite(stepPin, LOW);
    delayMicroseconds(500);
  }

  delay(1000);
}

```



## New board:
I mill new board because the FTDI was not working, I think the reason why the FTDI not worked, was because the traces was damaged. as shown in [Interface and application](http://fabacademy.org/2020/labs/uae/students/ali-binghulaita/assignments/week16/) programming assignment I use the FTDI header and worked very well.
This exactly the same compounts previous one but one of the changes I have in my new board, I did a built-in motor drive instead of having a separated board of the motor drive.
### Schematic:



![1](../images/week20/Schematic new1.jpg)



### Layout:


![1](../images/week20/Layout new1.jpg)






### NEW BOARD AND OLD BOARD

![1](../images/week20/new board2 (1).jpg)
![1](../images/week20/new board2.jpg)





### Adjust A4988 stepper motor driver


<iframe width="560" height="315" src="https://www.youtube.com/embed/zmZenc9766E" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

their was problem with A4988 motor drive, whenever i press the heat sink or the RST pin in the Driver, the stepper motor was working and noise sound come from the motor.
To fix the problem I connect the RST pin with SLP pin in the motor drive i add solder to connect the pins.
The schematic of A 4988 motor drive:


![](https://lastminuteengineers.com/wp-content/uploads/arduino/A4988-Stepper-Motor-Driver-Pinout.png)




This video display the problem and how i fix it, how in the end the stepper motor **work**:



<iframe width="560" height="315" src="https://www.youtube.com/embed/KNPSAG56haI" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>





### **3D designing and Printing**


I took the measurements of the camera by measuring the screw hole and diameter of the camera holder and then I design the piece.

![1](../images/week20/222222222222222.jpg)

and then printed the piece.

![1](../images/week20/20.jpg)
![1](../images/week20/21.jpg)
![1](../images/week20/22.jpg)

I design the camera holder and  printing the camera holder using the 3D printer.


![1](../images/week20/1111111111111111.jpg)

![1](../images/week20/23.jpg)
![1](../images/week20/24.jpg)

I inputted the measurements of the camera safety holder and the result is shown above.

![1](../images/week20/25.jpg)
![1](../images/week20/26.jpg)

I inputted the measurements of the camera tube holder and the results are shown above.

After I finished printing the parts I tried the camera holder by placing a wooden board between the camera holders to see if they fit in properly.

![1](../images/week20/20-31.jpg)

I assembled the pieces that I made onto each other. The camera tube onto the camera holder connected to the camera case.

![1](../images/week20/20-32.jpg)

After making sure that the pieces are connected properly I tried assmeblying them in the Plywood case and tried to fit them in neatly.

![1](../images/week20/20-33.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/hJtJ6pevbdg" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### **challenges:**

The programming part was a challenge to me, and I faced difficulties in it as I didn’t have much experience. Producing the board was a bit hard, because the board kept having faults in it. Creating the case was not very hard, but putting it together took some physical effort.

### **Future development:**

For the future I would like the case to have another heat sensing camera to detect the different temperatures ranges from different parts of the room. I also want to add another camera for zooming in, and having a better view of the place.


### **Files**



[final project cases fusion 360 file](../images/week20/casefinal1.f3d)


[final project eagle File brd](../images/week20/finalbord1.brd)


[final project eagle File sch](../images/week20/finalbord1.brd)



[final project eagle File brd new](../images/week20/finalbord1 new new.brd)

[final project eagle File sch new](../images/week20/finalsch1 new new.sch)



[stopper motor eagle File ](../images/week20/motord.brd)

[Camera Foundation](../images/week20/cam-foundation.stl)

[Camera Holder](../images/week20/cam-holder22 v7.stl)

[Camera Safety Case](../images/week20/cam-safetycase22 v5.stl)


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
