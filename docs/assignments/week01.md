# 1. Principles and practices
# Portable Life streaming case
# For covering fire accident

### Introduction
Based on the problems we faced in the fire accidents. The idea of the portable life streaming case was the best solution for life streaming the accidents. Reaching to a places where the cameras on the vehicle can’t reach. The commanders can follow the accidents and give orders from the central operating room.

### _**Initial Sketch**_

![1](../images/week01/initial_sketch-1.png)



Things we will buy camera Modem Battery Hard case.

Things we will fabricate in Fablab 3d printer Brackets and the foundations  laser cut acrylic cutting Arduino input output and programing mokding battery mold.

### Applications:

We can use the portable life stream case for covering the accidents from all sides.
The commanders will have full view on the accidents to run and give the right orders.
The video will be taking form the accidents will also help in investigations after the accidents.
It helps the trainers before going to the field and facing a real accident.
Gives accurate and real time intelligence as to the progression of the fire spread.

### Conclusion:

The idea of the portable life streaming case was to make it easy for the crew in the accident to move while they are caring the case and to ensure accident is covered from all sides easily.

 This is my idea which I will be working on in FABLAB academy in the next weeks.


___________________________________________________________________________________________


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
