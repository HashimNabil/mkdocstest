# 17. Machine and mechanical design

In this week, we worked in a group to design and build the . [foam cutting CNC machine](http://fabacademy.org/2020/labs/uae/mech/mech.html)





This week I worked on defining my final project idea and started to getting used to the documentation process.

For this week we were assigned to group projects and each student should take on one project individually. For my project I chose to make a Hot wire project for our group project which is a foam cutting machine.

### Making the Hot Wire

To make the hot wire project I have to prepare the wire that will be used to cut through foam easily and you can get the steel wire from this [website](https://www.desertcart.ae/products/58894627-100-meter-0-3-mm-fishing-stee-wire-nylon-coated-1-x-7-stainless-steel-leader-wire-super-soft-fishing-wire-lines-13-pound-test). I used steel wires which are the same as the ones used for fishing and when it current flows through it gets very hot.

![](../images/week17/17-1.jpg)

I need to use springs in this project because when the steel wire gets hot it bends and the spring can straighten the wire when it gets hot to help it cut foam better and neater. I also added a copper board between the steel wire and spring to act as a heat resistor and to prevent the spring from getting hot. If the spring gets hot it will bend also and will not make the wire straight for cutting.

I placed a small plastic part which is used to hold the spring onto the machine and is used for insulation also it is connected to the Y-axis of the machine.

In the video below I will explain what I will do for my first hot wire test. I connected the power supply to the dimmer and to the hot wire. I first used a 12 V power supply to test the voltage of the power supply and to see the required heat to cut the foam. The 12 V power supply did not give enough voltage and heat to the wire so I used an EFuel 24 V [power supply](https://www.worthpoint.com/worthopedia/skyrc-efuel-1200-watt-60-amp-dc-power-1927962489). to test the voltage and heat of the hot wire by using a foam to cut it using the wire.

![](../images/week17/17-2.jpg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Aei-rSRENN8" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

### Stainless steel rod holder

For my own part in the machine assignment, I designed the steel holder. It used to fix the two rods on the x-axis. This holder will be joined to the waleed part.



![](../images/week17/holder33.jpg)

this is the 3D design of the holder done by fusion, the file is in bottom of the page.

![](../images/week17/Stainless Steel Rod holder1111.jpg)



![](../images/week17/aaaaaaaa10000.jpg)

____________________________________________________________________________________________
[stainless steel rod holder Fusion 360 file ](../images/week17/groupmachine1x v16.f3d)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
