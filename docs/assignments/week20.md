# 20. Project development

### What tasks have been completed?

- Case design and fabrication milling the case in shopBot.

- Printing all 3d parts camera holder, camera mount, the tube holder.

- Cutting Acrylic base.

### What tasks remain?

- Work more on the programming.

### What’s working?

1. stepper motor is working.

2. RGB led strip is working.

3. modem system with camera.

### what’s not?

1. limit switch is not working

2. Power indicator  LED is not working

### What questions need to be resolved?

This is the question I want to resolve How to make the case to work for more hours? How to make a base for the battery charger? How can the camera rotate 360?

### What will happen when?

After finishing the diploma I will work more on this project.

### What have you learned?

I learnt to use the ideas I have to help people, and, I learnt to use different fabrication machines 3d printing, laser  machine, shopbot. before i do my project manual without using machines i use to do my projects without using 3d printing and laser cutter.

____________________________________________________________________________________________

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
