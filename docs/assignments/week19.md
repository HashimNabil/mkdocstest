# 19. Invention, Intellectual Property and Income

## **Individual Assignment**
________________________________________________________________________________________
Develop a plan for dissemination of your final project.



##Invention

The "Fire Eye" is the name of my final project. Based on the problems we faced in the fire accidents I thought of the idea of creating a portable life streaming case was the best solution for life streaming the accidents. Reaching to places where the cameras on the vehicle can’t reach, so the commanders can follow the accidents and give orders from the central operating room.

###Purpose
To live stream fire accidents to the commanders.

###Methods
This project is made using the lessons and skills I learned during my course of FabLab 2020.

##Intellectual Property
I decided to use an open source license and the website I used to create my license is  [Creative Commons](https://creativecommons.org/choose/#metadata).

![](../images/week19/19-1.jpg)

![](../images/week19/19-2.jpg)

![](../images/week19/19-3.jpg)

![](../images/week19/19-4.jpg)

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

With this license you are able to share your idea to other people and allow them to further adapt and reform the idea into a better one.

Under these following terms: You must give credit and provide a link of the license and show the changes that were made in the project. This project is not for commercial use in anyway. If the project was remade or used in any format you must provide the license as the original.


##Income

###Marketing:

When I made my license it  for Civil Defense purposes and we can make it for special cases.

###Manufacturing:

Manufacturing my project will be all locally and made in companies in the UAE.

###Future Possibilities:

I wish that my project gets recognized by government sectors so they can use it for their purposes.

![](../presentation.png)

____________________________________________________________________________________________

Group Assignment:Interface and Application programming: [link](http://fabacademy.org/2020/labs/uae/interface/interface.html).


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
