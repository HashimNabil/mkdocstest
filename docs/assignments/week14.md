# 14. Networking and communications

### **Individual Assignment:**

Design, build, and connect wired or wireless node(s) with network or bus addresses.

### **Group Assignment:**

Send a message between two projects.

please check the group assignment  [here.](http://fabacademy.org/2020/labs/uae/networking/networking.html)




For this assignment we are required to make a network using the Arduino Uno boards and
connecting them together.

There are 3 ways you can make a network using the  board.

1- Serial Networking

2- SPI Networking

3- I2C Networking

I picked the I2C circuit since it is  requires only few wires to connect them to
the board unlike the other networking methods.

I connected the wires using a solider to the Arduino Uno board. The connectors are connected to
the VCC and Ground ports on the board and they function as the Master and Slave.-

![1](../images/week14/net44.jpg)
![1](../images/week14/net22.jpg)

I made this support stand for the two Arduino boards that I will use for my assignment. They
will act as a support for the Master and Slave boards.


![1](../images/week14/net11.jpg)




As shown in the picture the connectors of the board are as follows:

SDA: Green

SCL: Yellow

VCC : Red

and Ground : Black



I connected the wires of both of the board to a single battery to make it easier instead of
using two batteries for each board.

![1](../images/week14/net33.jpg)

### **Master Code**
```
#include <Wire.h> //add the library for communication

void setup()
{
  Wire.begin(); // start the communiction
}

void loop()
{
  Wire.beginTransmission(1); // start transmitting to device 1
  Wire.write('H'); // send the letter "H" on the transmission line
  Wire.endTransmission(); //end the transmission

  delay(500);// delay for half a second

  Wire.beginTransmission(1); // transmit to device
  Wire.write('L');  // send the letter "L"
  Wire.endTransmission();  // end transmission

  delay(500); //wait for 0.5 sec.

  Wire.beginTransmission(2); //start transmission to device 2
  Wire.write('H'); //send "H"
  Wire.endTransmission(); //end transmission

  delay(500);  //wait for 0.5 sec

  Wire.beginTransmission(2); //start transmitting to device 2
  Wire.write('L');   // send "L"
  Wire.endTransmission();

  delay(500);  //delay for 0.5 sec
}
```

### **Slave Code**

```
#include <Wire.h> //add the library for communication
const byte slaveId = 1;// set this device as slave 1

void setup()
{
  Wire.begin(slaveId);// begin transmission for slave 1
  Wire.onReceive(receiveEvent);//register event

  pinMode(13,OUTPUT);//set pin 13 as output
  digitalWrite(13,LOW);// set pin 13 as low or 0
}
void loop()
{
}
void receiveEvent(int howMany)
{
  char inChar;// set a variable in characters
  while(Wire.available() > 0)//check if the transmission is available
  {
    inChar = Wire.read(); //read the data in the transmission line
    if (inChar == 'H') // check if the data is the letter "H"
    {
      digitalWrite(13, HIGH);// then digital write pin 13  as HIGH
    }
    else if (inChar == 'L')// if the data is "L"
    {
      digitalWrite(13, LOW);// then write pin 13 as LOW
    }
  }
}

```

### **Demonstration video**

<iframe width="560" height="315" src="https://www.youtube.com/embed/5JcYu9aqMsc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


_________________________________________________________________________________________

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
