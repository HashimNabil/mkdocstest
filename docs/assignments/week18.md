# 18. Wildcard week

This week I worked on defining my final project idea and started to getting used to the documentation process.

## **Individual Assignment**
________________________________________________________________________________________
Design and produce something with a digital fabrication process (incorporating computer-aided design and manufacturing) not covered in another assignment, documenting the requirements that your assignment meets, and including everything necessary to reproduce it. Possibilities include (but are not limited to):


###Working on Janome Memory Craft 15000

In the wildcard week I learned about Digital Fabrication using Janome Memory Craft 15000. First thing I needed to know is how to use the machine and from that I learned how to operate it from Salama.

Now I have to start by creating a file on the application Digitizer V5 Embroidery Software. This application is already available and downloaded onto the computers in FabLab.

For my design I decided to use the Embroidery hoop FA10 and the design I used is in the picture below.

![1](../images/week18/18-10.jpg)
![1](../images/week18/18-12.jpg)

Now since I already made my design I now have to open Digitizer V5 Embroidery Software and click on "Toolboxes" then "Auto-Digitizer" and the "Insert Artwork".

![1](../images/week18/18-13.jpg)

###Making the Design

Now I need to select the image and click on "Click-to-fill" and after it I place the image of the fabric which is called a "Kandora". I chose "Tatami stitch" because the Arabic letters are not available in the software.

![1](../images/week18/18-11.jpg)

The instructions I prepared for the Embroidery machine are shown below and I got these instructions from the PDF file available in this [website](https://www.janome.com/siteassets/support/manuals/embroidery-models/inst-mc15000-enlr2.pdf).

![1](../images/week18/18-4.jpg)

![1](../images/week18/18-5.jpg)

###Preparing the Machine

Here I placed the Embroidery Hoop onto the Kandora to start stitching my design on it.

![1](../images/week18/18-3.jpg)

I opened the software and moved the design inside the red box .

![1](../images/week18/18-14.jpg)

![1](../images/week18/18-15.jpg)

Now I have to click on "Save Design as"

![1](../images/week18/18-16.jpg)

Now I have to save the file on a flash memory so I can start stitching my Kandora on the machine.

![1](../images/week18/18-17.jpg)

###Stitching Process

I opened the machine and clicked on my design which is called "Design1".

![1](../images/week18/18-18.jpg)

After choosing the design and embroidery hoop the machine showed me a notice to confirm the correct embroidery hoop "FA10" I will use and after that I clicked "Ok".

![1](../images/week18/18-19.jpg)

The video showing how to put the thread in the machine. Also, I used the machine to automatically insert the thread in the needle hole

<iframe width="560" height="315" src="https://www.youtube.com/embed/VUP-FebQvlo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>







I entered the command on the machine to start stitching. I pressed on the Green button to start the stitching process.

![1](../images/week18/janome-15000.jpg)

![1](../images/week18/18-1.jpg)

This is the result after stitching my design onto the Kandora as shown in the picture below.

![1](../images/week18/18-2.jpg)
![1](../images/week18/18-6.jpg)

____________________________________________________________________________________________
[My  file in these week](../images/week18/Design1.DST)

[My  file in these week ](../images/week18/Design11.EMB)


<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.
